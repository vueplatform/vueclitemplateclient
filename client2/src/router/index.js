import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import HeaderBar from "@/components/common/HeaderBar.vue";
import FooterBar from "@/components/common/FooterBar.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/footerbar",
    name: "FooterBar",
    component: FooterBar
  },
  {
    path: "/headerbar",
    name: "HeaderBar",
    component: HeaderBar
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  }
  
];

const router = new VueRouter({
  routes
});

export default router;
