export const state = {
    variant:'info'
}

export const getters = {
    variant: state => state.variant
}

export const mutations = {
    variant: (state, variant) => {
        state.variant = variant
    }
}

export const actions = {
    variant(context, data) {
        context.commit('variant',data)
    }
}

export default {
    state,
    getters,
    mutations,
    actions
}